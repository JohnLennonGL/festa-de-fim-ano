package com.example.app2festadefimdeano.data;

import android.content.Context;
import android.content.SharedPreferences;

public class SecurityPreferences {
   private SharedPreferences mSharedPreferences;

   public SecurityPreferences(Context mcontext){


       this.mSharedPreferences = mcontext.getSharedPreferences("FestaFimAno",Context.MODE_PRIVATE);


   }

   public void storeString(String key,String Value){
       this.mSharedPreferences.edit().putString(key,Value).apply();
   }
   public String getStoredString (String key){
       return this.mSharedPreferences.getString(key,"");
   }
}
