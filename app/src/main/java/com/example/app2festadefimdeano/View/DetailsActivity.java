package com.example.app2festadefimdeano.View;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.CheckBox;
import android.widget.ImageButton;

import com.example.app2festadefimdeano.R;
import com.example.app2festadefimdeano.constant.festaFimAnoConstant;
import com.example.app2festadefimdeano.data.SecurityPreferences;

public class DetailsActivity extends AppCompatActivity  implements View.OnClickListener{

    private ViewHolder mViewHolder = new ViewHolder();
    private SecurityPreferences mSecurityPreferences;
private ImageButton mVoltar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details);

        mVoltar = findViewById(R.id.imageButtonID);
        mVoltar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        this.mSecurityPreferences = new SecurityPreferences(this);
       this.mViewHolder.checkparticipe = findViewById(R.id.check_participe);
        this.mViewHolder.checkparticipe.setOnClickListener(this);
        this.loadDataFromActvity();
    }



    @Override
    public void onClick(View v) {
        if(v.getId() == R.id.check_participe){

            if(this.mViewHolder.checkparticipe.isChecked()){
                //p
                this.mSecurityPreferences.storeString(festaFimAnoConstant.PRESENCE_KEY,festaFimAnoConstant.CONFIRMATION_YES);
            }
            else{
                //a
                this.mSecurityPreferences.storeString(festaFimAnoConstant.PRESENCE_KEY,festaFimAnoConstant.CONFIRMATION_NO);
            }
        }
    }

    private void loadDataFromActvity()
    {
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            String presence = extras.getString(festaFimAnoConstant.PRESENCE_KEY);
            if(presence != null && presence.equals(festaFimAnoConstant.CONFIRMATION_YES)){
                this.mViewHolder.checkparticipe.setChecked(true);
            } else{
                this.mViewHolder.checkparticipe.setChecked(false);
            }
        }
    }
    public static class ViewHolder {
        CheckBox checkparticipe;
    }
}
