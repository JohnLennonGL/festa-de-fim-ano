package com.example.app2festadefimdeano.View;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.example.app2festadefimdeano.R;
import com.example.app2festadefimdeano.View.DetailsActivity;
import com.example.app2festadefimdeano.constant.festaFimAnoConstant;
import com.example.app2festadefimdeano.data.SecurityPreferences;

import java.text.SimpleDateFormat;
import java.util.Calendar;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private ViewHolder mViewHolder = new ViewHolder();
    private SecurityPreferences mSecurityPreferences;
    private static SimpleDateFormat SIMPLE_DATE_FORMAT = new SimpleDateFormat("dd/MM/yyyy"); /// inserir data automatica

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        this.mSecurityPreferences = new SecurityPreferences(this);

        this.mViewHolder.textToday = findViewById(R.id.text_today);
        this.mViewHolder.textDiasRestantes = findViewById(R.id.dias_restantes);
        this.mViewHolder.BotaoEscolha = findViewById(R.id.Botao_Escolha);

        this.mViewHolder.BotaoEscolha.setOnClickListener(this);

       this.mViewHolder.textToday.setText(SIMPLE_DATE_FORMAT.format(Calendar.getInstance().getTime())); // inserir data automatica
        String daysLeft = String.format("%s %s", String.valueOf(this.getDaysLeft()), getString(R.string.dias)); // contagem de dias restantes
        this.mViewHolder.textDiasRestantes.setText(daysLeft); // contagem de dias restantes

        this.verifypresence();
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onResume() {
        super.onResume();
        this.verifypresence();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    // funcao contagem de ano
    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.Botao_Escolha) {

            String presence = this.mSecurityPreferences.getStoredString(festaFimAnoConstant.PRESENCE_KEY);

            Intent intent = new Intent(this, DetailsActivity.class);
            intent.putExtra(festaFimAnoConstant.PRESENCE_KEY,presence);
            startActivity(intent);
        }
    }


    private void verifypresence(){
        // nao confirmado / sim / nao
        String presence = this.mSecurityPreferences.getStoredString(festaFimAnoConstant.PRESENCE_KEY);
                if(presence.equals("")) {
                    this.mViewHolder.BotaoEscolha.setText(getString(R.string.nao_confirmado));
                } else if(presence.equals(festaFimAnoConstant.CONFIRMATION_YES)){
                    this.mViewHolder.BotaoEscolha.setText(getString(R.string.sim));
                }else{
                    this.mViewHolder.BotaoEscolha.setText(getString(R.string.nao));
                }
    }

    private  int getDaysLeft(){

        //Dia Atual do Ano
        Calendar diaAtualdoAno = Calendar.getInstance();
        int Atual = diaAtualdoAno.get(Calendar.DAY_OF_YEAR);


        Calendar diaMax = Calendar.getInstance();
        int Max = diaMax.getActualMaximum(Calendar.DAY_OF_YEAR);

        return Max - Atual;
    }

    private static class ViewHolder {

        TextView textToday;
        TextView textDiasRestantes;
        Button BotaoEscolha;

    }

}